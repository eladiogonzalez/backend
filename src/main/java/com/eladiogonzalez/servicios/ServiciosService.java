package com.eladiogonzalez.servicios;

import com.mongodb.*;
import com.mongodb.client.*;
import com.mongodb.client.MongoClient;
import org.bson.Document;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ServiciosService {
    static MongoCollection<Document> servicios;

    private static MongoCollection<Document> getServiciosCollection() {
        ConnectionString cs = new ConnectionString("mongodb://localhost:27017");
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(cs)
                .retryWrites(true)
                .build();
        MongoClient mongo = MongoClients.create(settings);
        MongoDatabase database = mongo.getDatabase("dbprod");
        return database.getCollection("servicios");
    }
    /*public static void insert(String servicio) throws Exception {
        Document doc = Document.parse(servicio);
        servicios = getServiciosCollection();
        servicios.insertOne(doc);
    }
    */
    //insercion por json o llamadas inserciones batch
    // se genero la funcion insert para validar si es insercion batch o simpe
    public static void insert(String strServicios) throws Exception {
        servicios = getServiciosCollection();
        Document doc = Document.parse(strServicios);
        List<Document> lst = doc.getList("servicios", Document.class);
        // se valida que lst sea nulo, ya que al usar getList le estamos indicando que palabra clave contiene
        // la lista en nuestro caso el json viene con la palabra servcio
        if (lst == null) {
            servicios.insertOne(doc);
        } else {
            servicios.insertMany(lst);
        }

    }


    public static List getAll() {
        servicios = getServiciosCollection();
        List lst = new ArrayList();
        FindIterable<Document> iterDoc = servicios.find();
        Iterator it = iterDoc.iterator();
        while (it.hasNext()) {
            lst.add(it.next());
        }
        return lst;
    }
    public static List getFiltrados(String filtro){
        servicios = getServiciosCollection();
        List lst = new ArrayList();
        Document docFiltro = Document.parse(filtro);
        FindIterable<Document> iterDoc = servicios.find(docFiltro);
        Iterator it = iterDoc.iterator();
        while (it.hasNext()) {
            lst.add(it.next());
        }
        return lst;
    }

    public static void update(String filtro, String updates) {
        servicios = getServiciosCollection();
        Document docFiltro = Document.parse(filtro);
        Document doc = Document.parse(updates);
        servicios.updateOne(docFiltro, doc);
    }


}
