package com.eladiogonzalez;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SercivioProductosApplication {

	public static void main(String[] args) {
		SpringApplication.run(SercivioProductosApplication.class, args);
	}

}
