package com.eladiogonzalez.productos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {
    //autowired es la manera de inyectar para tenerla disponible
    @Autowired
    ProductoRepository productoRepository;

    //consulta devuelve todos
    public List<ProductoModel> findAll(){
        return productoRepository.findAll();
    }


    //devolver si o no, opcional
    public Optional<ProductoModel> findById(String id){
        return productoRepository.findById(id);
    }

    //insertar informacion
    public ProductoModel save (ProductoModel prod){
        return productoRepository.save(prod);
    }

    //la funcion delete no devuelve nada, le agregamos logica de negocio
    public boolean delete(ProductoModel prod){
        try {
            productoRepository.delete(prod);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
    public Empleado getEmpleado(int id){
        for (Empleado emp: list.getListaEmpleados()){
            if (emp.getId() == id){
                return emp;
            }
        }
        return null;
    }

}







