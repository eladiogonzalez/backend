package com.eladiogonzalez.productos;

//se cambia class por interface, ya que se ejecutaran son metodos para la bd

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ProductoRepository extends MongoRepository<ProductoModel, String> {

}
