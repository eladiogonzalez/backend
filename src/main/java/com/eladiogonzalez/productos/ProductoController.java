package com.eladiogonzalez.productos;

import com.eladiogonzalez.productos.ProductoModel;
import com.eladiogonzalez.productos.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import java.util.List;

//hacemos que la clase sea un controlador

@RestController
//agragmos la ruta al invocar el servicio se debe colocar el apitechu/v2
@RequestMapping("/apitechu/v2")

public class ProductoController {
    @Autowired
    ProductoService productoService;

    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        return productoService.findAll();
    }

    @PostMapping("/productos")
    public ProductoModel postProducto(@RequestBody ProductoModel prod){
        productoService.save(prod);
        return productoService.save(prod);
    }
    @PutMapping("/productos")
    public void putProductos(@RequestBody ProductoModel prod){
        productoService.save(prod);
    }

    @DeleteMapping("/productos")
    public boolean deleteProductos(@RequestBody ProductoModel prod){
        return productoService.delete(prod);
    }

}
